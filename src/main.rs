use std::fmt::Display;

fn main() {
    println!("This program will find the point where loops using floating point precision numbers get stuck and break your program.");

    let mut has_run = false;
    for kind in std::env::args().skip(1) {
        has_run = true;

        match &*kind {
            "f32" => spawn_runner::<f32>((1u64 << 22) as f32),
            "f64" => spawn_runner::<f64>(((1u64 << 53) - (1u64 << 16)) as f64),
            other => println!("invalid option {:?}", other),
        }
    }
    if has_run == false {
        println!("\n !!!! Please supply at least one argument: f32 or f64 !!!!");
        std::process::exit(1);
    }
}

fn spawn_runner<T: num_traits::Float + std::ops::AddAssign + Display>(start: T) {
    let max = T::max_value();
    let one: T = T::one();

    let mut i = start;
    println!("\n         starting with {}", i);
    while i < max {
        let last = i;
        i += one;

        if i == last {
            print_fully(i);
            break;
        }
    }
}

fn print_fully<T: num_traits::Float + num_traits::NumCast + Display>(i: T) {
    let int = i
        .to_u128()
        .map(|i| format!("0x{:x}", i))
        .expect("print_fully cast Float -> u128 failed");
    let l2 = i.log2();
    println!("reached equilibrium at {} ({} == 2^{})", i, int, l2);
}
