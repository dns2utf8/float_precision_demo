# float_precision_demo

This program will find the point where loops using floating point precision numbers get stuck and break your program.

It has an optimisation to start near edge of where it gets stuck.
So the result will be the same as if the counting starts at zero, it is just way faster:

```
         starting with 4194304
reached equilibrium at 16777216 (0x1000000 == 2^24)

         starting with 9007199254675456
reached equilibrium at 9007199254740992 (0x20000000000000 == 2^53)
```

Reading equilibrium in this context means: adding one to the current number does not yield any effect anymore.

If you are coming from C 

```C
for (float i = 0; i < max; i++) { ... }
```
